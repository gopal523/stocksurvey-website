import { createRouter, createWebHistory } from "vue-router";
import LoginPage from "./components/LoginPage.vue";
import DashBoard from "./components/DashBoard.vue";
import MyProfile from "./components/MyProfile.vue";
import AttendanceTracker from "./components/AttendanceTracker.vue";
import ActivityReporting from "./components/ActivityReporting.vue";
import DownloadReport from "./components/DownloadReport.vue";
import StateWisePhase from "./components/StateWisePhase.vue";
import FinalResultPage from "./components/FinalResultPage.vue";


const routes = [
  {
    path: "/",
    name: "login",
    component: LoginPage,
  },
  {
    path: "/dashboard",
    name: "dashboard",
    component: DashBoard,
  },
  {
    path: "/myprofile",  
    name: "myprofile",
    component: MyProfile,
  },
  {
    path: "/attendancetracker",  
    name: "attendancetracker",
    component: AttendanceTracker,
  },
  {
    path: "/activityreporting",  
    name: "activityreporting",
    component: ActivityReporting,
  },
  {
    path: "/downloadreport",  
    name: "downloadreport",
    component: DownloadReport,
  },
  {
    path: "/statewisephase",  
    name: "statewisephase",
    component: StateWisePhase,
  },
  {
    path: "/finalresultpage",  
    name: "finalresultpage",
    component: FinalResultPage,
  },
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});


export default router;
