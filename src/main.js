import { createApp } from "vue";
import App from "./App.vue";
import router from "./routes";
import "bootstrap/dist/css/bootstrap.min.css"
import "bootstrap"
import VueSidebarMenu from 'vue-sidebar-menu'
import 'vue-sidebar-menu/dist/vue-sidebar-menu.css'


const app = createApp(App);
app.use(VueSidebarMenu);
app.use(router);
app.mount('#app')

//createApp(App).use(router).mount("#app");
